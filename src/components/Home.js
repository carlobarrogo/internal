import React, {useState} from 'react'
import '../App.css';
import '../adDash.css'
import LoginSample from '../components/LoginSample'
import OtherDashBoard from '../components/OtherDashBoard'


export const Home = () => {
    const [formIsSubmitted, setFormIsSubmitted] = useState(false);

    const submitForm =() =>{
     setFormIsSubmitted(true)
    }
    return (
        <div>
           { !formIsSubmitted ? <LoginSample submitForm ={submitForm}/> : <OtherDashBoard/>}
        </div>
            //   <LoginSample /> <OtherDashBoard/>  
    )
}
export default Home;