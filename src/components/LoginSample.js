import React , {useState,useEffect}from 'react'
import '../App.css'
import Lottie from 'react-lottie';
import animationData from '../animations/frontsvg.json'
import wave from '../animations/wave.json'
import admin from '../animations/admin.json'

import validation from './validation'


export const LoginSample = ({submitForm}) => {
  const [loading, setLoading] = useState(false); //loadingscreen
  const [values,setValues] = useState ({   //variables
      username: "",
      password:"",
  });

  const handleChange=(e)=>{                 
      setValues({
        ...values,
        [e.target.name]: e.target.value, //set values
      })
  };
  const [errors,setErrors]= useState({});
  const [dataIsCorrect, setDataIsCorrect]=useState(false);

  const signInButton = (e) => {

    e.preventDefault();
    setErrors(validation(values)); //get values for validation
    setDataIsCorrect(true); //if data is correct go to use effect
    setLoading(true) //loading screen to redirect to log in page again
    setTimeout(()=>{
      setLoading(false)

    }, 4500)
    
  };
  useEffect (()=> {
    if(Object.keys(errors).length === 0 && dataIsCorrect){ //if no errors; go to home
      setLoading(true)
      setTimeout(()=>{
        submitForm(true)
  
      }, 4000)

        
    }
    
},[errors]);
 

 

    const animationOptions = {
        loop: false,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
          preserveAspectRatio: 'xMidYMid slice',
        },
      };
      const waveOptions = {
        loop: true,
        autoplay: true,
        animationData: wave,
        rendererSettings: {
          preserveAspectRatio: 'xMidYMid meet',
        },
      };
      const adminOptions = {
        loop: false,
        autoplay: true,
        animationData: admin,
        rendererSettings: {
          preserveAspectRatio: 'xMidYMid meet',
        },
      };
    


    return (
      
        <div className="login"> 
        {
          loading ? <div className="loadingscreen" > <Lottie loading={loading} options={waveOptions} height={500} width={400}/></div>

          :
        <>
          <div className="pageText">
               <h1 className="Heading">WELCOME</h1>
               <h2 className="subHeading">QSERV-MCSU ADMIN</h2>
        </div>
        <div className="logform"  >
           <form>
           <div className="lottieadmin" > <Lottie  options={adminOptions} height={200} width={200}/></div>
           <input type="text" placeholder="Enter your username" name ="username" value={values.username} onChange={handleChange}></input>
           {errors.username && <span className="error">  {errors.username}</span>}
           <input type="password" placeholder="Enter password" name="password" value={values.password}  onChange={handleChange}></input>
           {errors.password && <span className="error"> {errors.password}</span>}
           <button type="button" className="loginbttn" onClick={signInButton} >Sign in</button>
           </form>
         
        </div>
        <div className="lottiewave" > <Lottie  options={waveOptions} height={500} width={400}/></div>
        <div className="lottieanim" > <Lottie  options={animationOptions} height={500} width={900}/></div>   
        </>   
        }
   
       </div>
   
    )
}
export default LoginSample