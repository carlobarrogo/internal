import React from 'react'
import CountUp from 'react-countup'
import '../App.css'
import Lottie from 'react-lottie';
import logo from '../animations/logo.json'
import plant from '../animations/plant.json'
import working from '../animations/working.json'
import assets from '../animations/assets.json'
import Table from '../components/Table'


export const OtherDashBoard = () => {


  const logoOptions = {
    loop: false,
    autoplay: true,
    animationData: logo,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid meet',
    },
  };
  const plantOptions = {
    loop: true,
    autoplay: true,
    animationData: plant,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid meet',
    },
  };
  const workingOptions = {
    loop: true,
    autoplay: true,
    animationData: working,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid meet',
    },
  };
  const assetsOptions = {
    loop: true,
    autoplay: true,
    animationData: assets,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid meet',
    },
  };

    return (
        <div className="grid-container">
          <div className="menu-icon">
          <i className="fas fa-bars header__menu"></i>
        </div>
          
        <header className="header">
          <div className="header__search"><input type="text"  placeholder="Search.." name="search" /></div>
          <div className="header__avatar">Your face</div>
          
        </header>
        
        <aside className="sidenav">
        <div className="logo-details">
           <i className="lottielogo" ><Lottie  options={logoOptions} height={100} width={50}/></i>
           <span className="logo_name">QSERV-MCSU </span>
          </div>
          
          <ul className="sidenav__list">
            <li className="sidenav__list-item active">Dashboard</li>
            <li className="sidenav__list-item ">Employee Management</li>
            <li className="sidenav__list-item">Asset Management</li>
            <li className="sidenav__list-item">Project Management</li>
         
          </ul>
        </aside>
      
        <main className="main">
          <div className="main-header">
            <div className="welcome">
            <div class="welcome-title ">Welcome, <strong>Admin</strong></div>
          <div class="welcome-subtitle ">How are you today?</div>

            </div>
            
            
          </div>
      
          <div className="main-overview">
            <div className="overviewcard">
              <div className="overviewcard__icon"><Lottie  options={plantOptions} height={150} width={180}/></div>
              <div className="overviewcard-description">
          <h3 className="overviewcard-title text-light">Members</h3>
          <p className="overviewcard-subtitle"><CountUp end={62} duration={7}/></p>
        </div>
              
            </div>
            <div className="overviewcard">
              <div className="overviewcard__icon"><div className="workinglottie"><Lottie  options={workingOptions} height={170} width={200}/></div></div>
              <div className="overviewcard-description">
          <h3 className="overviewcard-title text-light">Projects</h3>
          <p className="overviewcard-subtitle"><CountUp end={6} duration={7}/></p>
        </div>
              
            </div>
            <div className="overviewcard">
              <div className="overviewcard__icon"><div className="assetslottie"><Lottie  options={assetsOptions} height={170} width={150}/></div></div>
              <div className="overviewcard-description">
          <h3 className="overviewcard-title text-light">Assets</h3>
          <p className="overviewcard-subtitle"><CountUp end={132} duration={7}/></p>
        </div>
              
            </div>
            
          </div>
      
          <div className="cardfull">
           
            <Table />

          </div>
          <div className="main-cards">
            <div className="card">Card</div>
            <div className="card">Card</div>
            <div className="card">Card</div>
          </div>
        </main>
      
        <footer className="footer">
          <div className="footer__copyright">&copy; 2021</div>
          <div className="footer__signature">QSERVE-MCSU</div>
        </footer>
     </div>
    )
}
export default OtherDashBoard