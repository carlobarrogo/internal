import React ,{useState} from "react";
import { useTable, useGlobalFilter, useAsyncDebounce, usePagination } from 'react-table'
import '../adDash.css';




// Define a default UI for filtering
function GlobalFilter({ globalFilter,setGlobalFilter,}) {
    
  

    const [value, setValue] = useState(globalFilter)
    const onChange = useAsyncDebounce(value => {
        setGlobalFilter(value || undefined)
    }, 2000)

    return (
        <div className="search-container">
        <span className="searchfilter">
            {' '}
            <input className="search-input" value={value || ""} onChange={e => { setValue(e.target.value); onChange(e.target.value);}}
                placeholder= "Search on table"
            />
       
        </span>
        </div>
    )
}


function Table({ columns, data }) {

    

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        page,
        prepareRow,
        state,
        preGlobalFilteredRows,
        setGlobalFilter,
        canPreviousPage,
        canNextPage,
        pageOptions,
        pageCount,
        gotoPage,
        nextPage,
        previousPage,
        setPageSize,
        state: { pageIndex, pageSize },
    } = useTable(
        {
            columns,
            data,
            initialState: { pageIndex: 0 },
        },
       
        useGlobalFilter,
        usePagination
    );
    const [modelState, setModelState] = useState({
        showModel: false,
        data: null
      });



    return (
        
        <div>

             {modelState.showModel && (
        <div className="modalclassoverlay">
        <div className="modalclass">
        <div className="modalheader"><h1> Information {/*{console.log(modelState.data)} */} </h1></div>
        <div className="modalinfo">
       
        <div>{modelState.data.original.designation} - {modelState.data.original.fullName}  </div>

       
        <div>{modelState.data.original.deparment}   </div>
        <div>{modelState.data.original.sdept}   </div>
        </div>
           

 
  <button className="modalbttn" onClick={() =>
                    setModelState({ showModel: false })
                  }> Done</button>
                  </div>
    </div>
        
      )}
            <GlobalFilter
                preGlobalFilteredRows={preGlobalFilteredRows}
                globalFilter={state.globalFilter}
                setGlobalFilter={setGlobalFilter}
            />
              
            
            <table className="table" {...getTableProps()}>
                <thead>
                    {headerGroups.map(headerGroup => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map(column => (
                                <th className="headertable" {...column.getHeaderProps()}>
                                    {column.render('Header')}
                                    {/* Render the columns filter UI */}
                                    
                                </th>
                            ))}
                            <th>Actions</th>
                        </tr>
                    ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                    {page.map((row, i) => {
                        prepareRow(row)
                        return (
                            <tr  {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                    return <td className="rowtable"{...cell.getCellProps()}>{cell.render('Cell')}</td>
                                   
                                
                                })}
                                <td><div className="actions"><button className="editbttn" >Edit</button><button className="viewbttn"  onClick={() =>
                        setModelState({ showModel: true, data: row })
                      }>view</button><button className="deletebttn">Delete</button></div></td>
                               
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <br />
            <div className="pagination">
        <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
          {'<<'}
        </button>{' '}
        <button onClick={() => previousPage()} disabled={!canPreviousPage}>
          {'<'}
        </button>{' '}
        <button onClick={() => nextPage()} disabled={!canNextPage}>
          {'>'}
        </button>{' '}
        <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}> 
          {'>>'}
        </button>{' '}
        <span className="pageofpage">
          Page{' '}
          <strong>
            {pageIndex + 1} of {pageOptions.length}
          </strong>{' '}
        </span>
        <span className ="goto" >
          || Go to page:{' '}
          <input
            type="number"
            defaultValue={pageIndex + 1}
            onChange={e => {
              const page = e.target.value ? Number(e.target.value) - 1 : 0
              gotoPage(page)
              
            }}
            className = "gotopage"
          
          />
        </span>{' '}
        <select
          value={pageSize}
          onChange={e => {
            setPageSize(Number(e.target.value))
          }}
          className ="showpage"
        >
          {[10, 20, 30, 40, 50].map(pageSize => (
            <option key={pageSize} value={pageSize}>
              Show {pageSize}
            </option>
          ))}
        </select>
      </div>

        </div>
    )
}



function FilterTable() {
    const columns = React.useMemo(
        () => [
         
            {
                Header: 'Details',
                columns: [
                    {
                        Header: 'Full Name',
                        accessor: 'fullName',
                    },
                 
                    {
                        Header: 'Designation',
                        accessor: 'designation',
                    },
                    {
                        Header: 'Department',
                        accessor: 'deparment',
                    },
                    {
                        Header: 'Sub Department',
                        accessor: 'sdept',
                    },
                    {
                        Header: 'Sub Team',
                        accessor: 'steam',
                    },
                    {
                        Header: 'Start Date',
                        accessor: 'sdate',
                    },
                    {
                        Header: 'End Date',
                        accessor: 'edate',
                    },
                    
                    
                ],
            },
         
        ],
        []
    )

    const data = [
        {
            "fullName": "Carlo Barrogo",
            "deparment":"MCSU",
            "designation":"Jr Developer",
            "sdept": "Software Team",
            "steam": "Ralph Villa",
            "sdate": "05/17/2021",
            "edate": "05/16/2022",
            
        },
        {
            "fullName": "Dan Mikko Mazo",
            "deparment":"MCSU",
            "designation":"Jr Developer",
            "sdept": "Software Team",
            "steam": "Ralph Villa",
            "sdate": "05/17/2021",
            "edate": "05/16/2022",
        },
     
        {
            "fullName": "Erik Julius Castro",
            "deparment":"MCSU",
            "designation":"Jr Developer",
            "sdept": "Software Team",
            "steam": "Ralph Villa",
            "sdate": "05/17/2021",
            "edate": "05/16/2022",
        },
        {
            "fullName": "Ralph Villa",
            "deparment":"MCSU",
            "designation":"Sr Developer",
            "sdept": "Software Team",
            "steam": "Ralph Villa",
            "sdate": "05/17/2021",
            "edate": "05/16/2022",
        },
        {
            "fullName": "Mico Vital",
            "deparment":"MCSU",
            "designation":"Sr Developer",
            "sdept": "Software Team",
            "steam": "Ralph Villa",
            "sdate": "05/17/2021",
            "edate": "05/16/2022",
        },
        {
            "fullName": "Yuichi Gonzales",
            "deparment":"MCSU",
            "designation":"Technical Writer",
            "sdept": "Software Team",
            "steam": "Ralph Villa",
            "sdate": "05/17/2021",
            "edate": "05/16/2022",
        },
      
        
    ]

    return (
        <Table columns={columns} data={data} />
    )
}

export default FilterTable;