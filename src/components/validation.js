const validation = (values) => {

    let errors ={};

    if(!values.username){
        errors.username="Invalid Username"
    }
    if(!values.password){
        errors.password="Invalid Password"
    }

    return errors;

    
}

export default validation
